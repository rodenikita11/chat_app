<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Exhibitorstall extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $table = 'exhibitor_stalls';
    public $timestamps = false;

    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'stall_name',
        'exhibitor_id',
        'product_link',
        'video_link',
        'brochure_file',
        'visiting_card',
        'video_chat_link',
        'contact_email',
        'priority',
        'facebook',
        'instagram',
        'linkedin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created' => 'datetime',
        'modified' => 'datetime',
    ];
}
