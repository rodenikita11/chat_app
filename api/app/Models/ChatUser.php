<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatUser extends Model
{
    protected $table = 'chat_users';

    protected $primaryKey = 'id';
	
    protected $fillable = [
        'chat_unique_id',	'user_id', 'is_active'
    ];
}
