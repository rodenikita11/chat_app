<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    //
    protected $table = 'chats';

    protected $primaryKey = 'id';
	
    protected $fillable = [
        'unique_id', 'type', 'chat_type'
    ];
}
