<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    //
    protected $table = 'chat_messages';

    protected $primaryKey = 'id';
	
    protected $fillable = [
        'chat_unique_id',	'message',	'from_userid',	'is_read'
    ];
}
