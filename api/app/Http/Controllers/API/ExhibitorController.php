<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

class ExhibitorController extends BaseController
{
    private $pub_html;
    private $visitingCardPath;
    private $brochurePath;
    private $bgVideoPath;
    public function __construct(){
        $this->pub_html = "./storage/app/public/exhibitor_stall/";
        $this->visitingCardPath = $this->pub_html.'visiting_card/';
        $this->brochurePath = $this->pub_html.'brochure_file/';
        $this->bgVideoPath = "./storage/app/public/exhibitor_stall_video/";
    }
    
    public function index(Request $request){
        $searchQuery = "";
        $response = array();
        $columnName = 'priority';
        $columnSortOrder = "asc";
        $rowperpage = "-1";
        $draw = '1';
        
        $stalls = DB::table('exhibitor_stalls')
            ->join('users', 'users.id', '=', 'exhibitor_stalls.exhibitor_id')
            ->select(['exhibitor_stalls.*', 'users.name']);
        if($_SERVER['REQUEST_METHOD']=='POST'){
            $postData = $request->post();
            ## Read value
            $draw = $postData['draw'];
            $start = $postData['start'];
            $rowperpage = $postData['length']; // Rows display per page
            $columnIndex = $postData['order'][0]['column']; // Column index
            $columnName = $postData['columns'][$columnIndex]['data']; // Column name
            $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
            $searchValue = $postData['search']['value']; // Search value
            
            if($searchValue != ''){
               $searchQuery = " AND (t.name like '%".$searchValue."%' or t.product_link like '%".$searchValue."%' or t.video_link like '%".$searchValue."%' or t.video_chat_link like '%".$searchValue."%')";
            }
            //echo json_encode($request->all());exit;
            //$stalls->where();
        }
        
        $sql = $stalls->toSql();
        /*$sql = $stalls->toSql();
        //exit;
        $sql2 = 'Select count(*) as allcount from ('.$sql.') t';*/
        $records = DB::table('exhibitor_stalls')
            ->join('users', 'users.id', '=', 'exhibitor_stalls.exhibitor_id')
            ->select(['exhibitor_stalls.*', 'users.name'])->count();
        
        $totalRecords = $records;
        //echo $totalRecords;exit;
        
        $sql2 = 'Select count(*) as allcount from ('.$sql.') t where 1=1'.$searchQuery;
        $records = DB::select($sql2);
        //print_r($records);exit;
        $totalRecordwithFilter = $records[0]->allcount;
        $sql2 = 'Select * from ('.$sql.') t where 1=1'.$searchQuery.' order by '.$columnName.' '.$columnSortOrder;
        if ($rowperpage!='-1') {
            $sql2.=' LIMIT '.$start.', '.$rowperpage;
        }
        //echo $sql2;
        $records = DB::select($sql2);
        $data = array();
        foreach($records as $recordKey => $record ){
   
           $data[] = array(
            "sr_no" => $recordKey+1,
            "id"=>$record->id,
            "stall_name"=>$record->stall_name,
            "exhibitor_id"=>$record->exhibitor_id,
            "exhibitor_name"=>$record->name,
            "contact_email"=>$record->contact_email,
            "priority"=>$record->priority,
            "product_link"=>$record->product_link,
            "video_link"=>$record->video_link,
            "brochure_file"=>$this->brochurePath.$record->brochure_file,
            "visiting_card"=>$this->visitingCardPath.$record->visiting_card,
            "group_name"=>$record->group_name,
            "bg_video"=>$this->bgVideoPath.$record->bg_video,
            "facebook"=>$record->facebook,
            "instagram"=>$record->instagram,
            "linkedin"=>$record->linkedin,
            "video_chat_link"=>$record->video_chat_link,
            "created"=>($record->created!=='0000-00-00 00:00:00')?date('d-m-Y', strtotime($record->created)):'',
            "created_by"=>$record->created_by,
            "modified"=>($record->modified!=='0000-00-00 00:00:00')?date('d-m-Y', strtotime($record->modified)):'',
            "modified_by"=>$record->modified_by,
            "is_active"=>$record->is_active,
            'action'=>'Action'
           ); 
        }
        //echo "<pre>"; print_r($data);exit;
        ## Response
        $response = array(
           "draw" => intval($draw),
           "iTotalRecords" => $totalRecordwithFilter,
           "iTotalDisplayRecords" => $totalRecords,
           "aaData" => $data
        );
        
        return json_encode($response);
        //exit;
    }
    
    public function add_stall(Request $request){
        /*print_r($request->all());
        exit;*/
        $validator = Validator::make($request->all(), [
            'exhibitor_id' => 'required',
            'product_link' => 'required',
            'video_link' => 'required',
            //'brochure_file' => 'required',
            //'visiting_card' => 'required',
            'video_chat_link' => 'required',
            'contact_email' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        
        $postData = $request->all();
        if(isset($postData['id'])){
            $postData['created'] = date('Y-m-d H:i:s');   
            $postData['created_by']  = $request->user()->id;
        }else{
            $checkStall = DB::table('exhibitor_stalls')
            ->where('exhibitor_id', $postData['exhibitor_id'])
            ->first();
            //echo json_encode($checkStall);exit;
            if(null!==$checkStall){
                $postData['id'] = $checkStall->id;
            }
            $postData['modified'] = date('Y-m-d H:i:s');
            $postData['modified_by']  = $request->user()->id;
        }
        
        if (!empty($request->hasFile('brochure_file'))) { 
            //echo json_encode($request->file('brochure_file'));exit;
            $original_filename = $request->file('brochure_file')->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $file_ext = $request->file('brochure_file')->getClientOriginalExtension();
            $file_type = $request->file('brochure_file')->getMimeType();
            if($file_type == "application/pdf"){
            	$image = $original_filename_arr[0].time(). '.'.$file_ext;
                if ($request->file('brochure_file')->move($this->brochurePath, $image)) {
                    $postData['brochure_file'] = $image;
            	} else {
                    return json_encode(['message'=>'Unable To Upload Brochure File', 'status'=>'fail']);
                }
            }else{
                return json_encode(['message'=>'Enter Valid File Format for Brochure', 'status'=>'fail']);
            }
        }else{
            $postData['brochure_file'] = $postData['brochure_file2'];
        }
        
        if (!empty($request->hasFile('visiting_card'))) {
            $original_filename = $request->file('visiting_card')->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $file_ext = $request->file('visiting_card')->getClientOriginalExtension();
            $file_type = $request->file('visiting_card')->getMimeType();
            if($file_type == 'image/png' || $file_type == 'image/jpg' || $file_type == 'image/jpeg'){
            	$image = $original_filename_arr[0].time(). '.'.$file_ext;
                if ($request->file('visiting_card')->move($this->visitingCardPath, $image)) {
                    $postData['visiting_card'] = $image;
                } else {
                    return json_encode(['message'=>'Unable to upload Visiting Card', 'status'=>'fail']);
                }
            }else{
                return json_encode(['message'=>'Enter Valid File Format for Visiting Card', 'status'=>'fail']);
            }
        }else{
            $postData['visiting_card'] = $postData['visiting_card2'];
        }
        
        if(array_key_exists('visiting_card2', $postData)){ 
            unset($postData['visiting_card2']);
        }
            
        if(array_key_exists('brochure_file2', $postData)){
            unset($postData['brochure_file2']);
        }
        
        $stall = false;
        $message = 'Unknown Error';
        try { 
            if(isset($postData['id'])){
                $stall = DB::table('exhibitor_stalls')
                  ->where('id', $postData['id'])
                  ->update($postData);
                if($stall){
                    $message = "Stall Details Updated For This Exhibitor";
                }
            }else{
                $priority = DB::table('exhibitor_stalls')->count();
                $postData['priority'] = $priority +1;
                $stall = DB::table('exhibitor_stalls')->insertGetId($postData);
                if($stall){
                    $message = "Stall Assigned To This Exhibitor And Details Has Been Updated Successfully";
                }
            }
        } catch(\Illuminate\Database\QueryException $ex){ 
          //dd($ex->getMessage()); 
          return json_encode(['message'=>$ex->getMessage(), 'status'=>'fail']);
        }
        
        return json_encode(['message'=>$message, 'status'=>'success']);
        
    }
    
    public function view_stall($exhibitorId){
        $user = DB::table('users')->join('user_roles', 'user_roles.user_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'user_roles.role_id')->select('users.id', 'users.name', 'users.email', 'users.mobile', 'roles.role_name')->where(['roles.slug'=>"exhibitor", 'users.id'=>$exhibitorId])->get();
        if(count($user)>0){
            $exhibitorStall = DB::table('exhibitor_stalls')->select(['exhibitor_stalls.*', 'users.name'])->join('users', 'users.id', '=', 'exhibitor_stalls.exhibitor_id')->where('exhibitor_id', $exhibitorId)->first();
            $exhibitorStall->brochure_path = $this->brochurePath;
            $exhibitorStall->visiting_card_path = $this->visitingCardPath;
            if(NULL!==$exhibitorStall){
                //return json_encode(['message'=>'Bingo, Stall Assigned To This Exhibitor', 'status'=>'success', 'data'=>$exhibitorStall, 'statusCode'=>200]);
                return $this->sendResponse($exhibitorStall, 'Success');
            }else{
                return json_encode(['message'=>'It Seems No Stall Assigned To This Exhbitor', 'status'=>'fail', 'statusCode'=>1001]);
            }
        }else{
            return json_encode(['message'=>'Invalid Exhibitor', 'status'=>'fail', 'statusCode'=>1000]);
        }
    }
    
    public function reorder_stall(Request $request){
        if($_SERVER['REQUEST_METHOD']=="POST"){
            $postData = $request->all();
            //echo "reached in API";
            //print_r($postData);
            try{
                DB::table('exhibitor_stalls')->update(['priority'=> null]);
                foreach($postData['stalls'] as $key => $value){
                    $update['priority'] = $value['priority'];
                    $update['modified'] = date('Y-m-d H:i:s');
                    $update['modified_by'] = $request->user()->id;
                    //DB::table('exhibitor_stalls')->where('id',$value['id'])->update(['priority'=>0]);
                    DB::table('exhibitor_stalls')->where('id',$value['id'])->update($update);
                    //print_r($value);
                }
            
            }catch(\Illuminate\Database\QueryException $ex){ 
              //dd($ex->getMessage()); 
              return json_encode(['message'=>$ex->getMessage(), 'status'=>'fail']);
            }
            
            return json_encode(['message'=>'Stalls Reordered Successfully', 'status'=>'success']);
            //exit;
        }else{
            return json_encode(['message'=>'Invalid Access', 'status'=>'fail']);
        }
    }
    
    public function add_stall_group(Request $request){
        //echo '<pre>';print_r($_SERVER);exit;
        //echo '<pre>';print_r($request->all());exit;
        $validator = Validator::make($request->all(), [
            'exhibitor_id' => 'required',
            'group_name' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        
        $postData = $request->all();

        if(isset($postData['id']) ){
            $postData['modified'] = date('Y-m-d H:i:s');   
            $postData['modified_by']  = $request->user()->id;
        }else{
            $checkStall = DB::table('exhibitor_stalls')
            ->where('exhibitor_id', $postData['exhibitor_id'])
            ->first();
            //echo json_encode($checkStall);exit;
            if(null!==$checkStall){
                $postData['id'] = $checkStall->id;
                $postData['created'] = date('Y-m-d H:i:s');
                $postData['created_by']  = $request->user()->id;
            }
            $postData['modified'] = date('Y-m-d H:i:s');
            $postData['modified_by']  = $request->user()->id;
        }
        //print_r($postData);exit;
        if (!empty($request->hasFile('bg_video'))) { 
            //echo $this->bgVideoPath;exit;
            //echo json_encode($request->file('brochure_file'));exit;
            $original_filename = $request->file('bg_video')->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $file_ext = $request->file('bg_video')->getClientOriginalExtension();
            $file_type = $request->file('bg_video')->getMimeType();
            if($file_type == "video/mp4"){
                $image = $original_filename_arr[0].time(). '.'.$file_ext;
                if ($request->file('bg_video')->move($this->bgVideoPath, $image)) {
                    $postData['bg_video'] = $image;
                } else {
                    return json_encode(['message'=>'Unable To Upload Video File', 'status'=>'fail']);
                }
            }else{
                return json_encode(['message'=>'Enter Valid File Format for Video', 'status'=>'fail']);
            }
        }else{
            $postData['bg_video'] = $postData['bg_video_2'];
        }
        
        //echo '<pre>';print_r($postData);exit;
        
        if(array_key_exists('bg_video_2', $postData)){ 
            unset($postData['bg_video_2']);
        }
            
        
        //echo '<pre>';print_r($postData);exit;
        $stall = false;
        $message = 'Unknown Error';
        try { 
            if(isset($postData['id'])){
                $stall = DB::table('exhibitor_stalls')
                  ->where('id', $postData['id'])
                  ->update($postData);
                if($stall){
                    $message = "Stall Details Updated For This Exhibitor";
                }
            }else{
                $priority = DB::table('exhibitor_stalls')->count();
                $postData['priority'] = $priority +1;
                $stall = DB::table('exhibitor_stalls')->insertGetId($postData);
                if($stall){
                    $message = "Stall Group Assigned To This Exhibitor And Details Has Been Updated Successfully";
                }
            }
        } catch(\Illuminate\Database\QueryException $ex){ 
          //dd($ex->getMessage()); 
          return json_encode(['message'=>$ex->getMessage(), 'status'=>'fail']);
        }
        
        return json_encode(['message'=>$message, 'status'=>'success']);
        
    }
}