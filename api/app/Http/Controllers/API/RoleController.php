<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

class RoleController extends BaseController
{
    //
    public function __construct(){

    }

    public function index(){
    	$roles = DB::table('roles')->select('*')->where('is_active', true)->paginate(10);
    	$data = [];
    	if(count($roles)>0){
    	    
    	    return $this->sendResponse($roles, 'User register successfully.');
    	}else{
    	    return $this->sendResponse($data, 'No data found.');
    	}
    	
    }

    public function add_role(Request $request){
        $input = $request->all();
    	//echo '<pre>';print_r($input);exit;
        $validator = Validator::make($request->all(), [
           // 'role_name' => 'required',
            'role_code' => 'required'/*,
            'role_name' => 'required|unique:roles,role_name'*/
        ]);
       // echo '<pre>';print_r($validator);exit;
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $checkRole = DB::table('roles')->select('role_name')->where('role_name', $input['role_name'])->first();
        if($checkRole){
            return $this->sendError('Role Already Exist.', $validator->errors());       
        }
        $postData['role_name'] = $input['role_name'];
        $postData['role_code'] = $input['role_code'];
        $postData['slug'] = trim(strtolower($input['role_name']));
        $postData['is_active'] = true;
        $postData['created_at'] = $postData['modified_at'] = date('Y-m-d H:i:s');
        
        
        
        //echo '<pre>';print_r($postData);exit;
    	$query = DB::table('roles')->insertGetId($postData);
    	if($query){
    	    $detail['role_id'] = $query;
    	    isset($input['details']['is_view'])?$detail['is_view'] = $input['details']['is_view']:'';
            isset($input['details']['is_add'])?$detail['is_add'] = $input['details']['is_add']:'';
            isset($input['details']['is_update'])?$detail['is_update'] = $input['details']['is_update']:'';
            isset($input['details']['is_delete'])?$detail['is_delete'] = $input['details']['is_delete']:'';
            $detail['is_active'] = true;
            $detail['created'] = $detail['modified_at'] = date('Y-m-d H:i:s');
    	    $result = ['message'=>'role created successfully', 'status'=>'success'];
    	    return json_encode($result);
    	}else{
    	    $result = ['message'=>'error occured while creating role', 'status'=>'fail'];
    	    return json_encode($result);
    	}
    	
    }
    
    public function edit_role(Request $request){
        $input = $request->all();
    	//echo '<pre>';print_r($input);exit;
        $validator = Validator::make($request->all(), [
            'role_name' => 'required',
            'role_code' => 'required',
            'slug'=>'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        isset($input['role_name'])?$postData['role_name'] = $input['role_name']:'';
        isset($input['role_code'])?$postData['role_code'] = $input['role_code']:'';
        isset($input['is_active'])?$postData['is_active'] = $input['is_active']:'';
        $postData['modified_at'] = date('Y-m-d H:i:s');
        $role = DB::table('roles')->where('slug', $input['slug'])->first();
        if($role){
            $query = DB::table('roles')->where('slug', $input['slug'])->update($postData);
           // print_r($query);exit;
        	if($query){
        	    isset($input['details']['is_view'])?$detail['is_view'] = $input['details']['is_view']:'';
                isset($input['details']['is_add'])?$detail['is_add'] = $input['details']['is_add']:'';
                isset($input['details']['is_update'])?$detail['is_update'] = $input['details']['is_update']:'';
                isset($input['details']['is_delete'])?$detail['is_delete'] = $input['details']['is_delete']:'';
                isset($input['details']['is_active'])?$detail['is_active'] = $input['details']['is_active']:'';
                $detail['modified_at'] = date('Y-m-d H:i:s');
                //print_r($detail);exit;
                $roleDetail = DB::table('role_details')->select('*')->where('role_id', $role->id)->first();
                if($roleDetail){
                    DB::table('role_details')->where('role_id', $role->id)->update($detail);
                }else{
                    $detail['role_id'] = $role->id;
                    $detail['created_at'] = $detail['modified_at'] = date('Y-m-d H:i:s');
                    DB::table('role_details')->insert($detail);
                }
                
        	    $result = ['message'=>'role updated successfully', 'status'=>'success'];
        	    return json_encode($result);
        	}else{
        	    $result = ['message'=>'error occured while updating role', 'status'=>'fail'];
        	    return json_encode($result);
        	}  
        }else{
            $result = ['message'=>'invalid slug', 'status'=>'fail'];
        	return json_encode($result);
        }
    	
    	
    }
    
    public function delete_role($id){
        //echo $id;exit;
    	$query = DB::table('roles')->where('id', $id)->delete();
    	if($query){
    	    $result = ['message'=>'role deleted successfully', 'status'=>'success'];
    	    return json_encode($result);
    	}else{
    	    $result = ['message'=>'error occured while deleting role', 'status'=>'fail'];
    	    return json_encode($result);
    	}
    	
    }
    
    public function view_role($slug){
        //echo $id;exit;
    	$query = DB::table('roles')->select('id','role_name', 'role_code','slug')->where('slug', $slug)->first();
    	
    	if($query){
    	    $detail = DB::table('role_details')->select('*')->where('role_id', $query->id)->get();
    	    $query->detail = $detail;
    	    //echo '<pre>';print_r($query);exit;
    	    $result = ['message'=>'role fetched successfully', 'status'=>'success', 'data'=>$query];
    	    return json_encode($result);
    	}else{
    	    $result = ['message'=>'no data exist', 'status'=>'fail'];
    	    return json_encode($result);
    	}
    	
    }
    
}
