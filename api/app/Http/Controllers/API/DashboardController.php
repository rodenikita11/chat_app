<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

class DashboardController extends BaseController
{
    public function __construct(){
	
    }

    public function index(Request $request, $type){
		$id = $request->user()->id;
		$data = [];
		
        if($type == "exhibitor"){
			$data["Total No of visitors"] = DB::table('users')->join('user_roles', 'user_roles.user_id', '=', 'users.id')
				->join('roles', 'roles.id', '=', 'user_roles.role_id')->select('users.*', 'roles.role_name')->where('roles.slug', "visitor")->count();
			//var_dump($data);die;
			
			$activities = ["view product","promo video", "brochure", "visiting card", "chats", "video chat","social media", "email"];
			foreach($activities as $activity){
				$data[$activity] = DB::table('visitor_logs')->where([ ['remark', 'like', '%'.$activity.'%'],['exhibitor_stall_id', '=', $id] ])->count();
				//$data[$activity] = $userscount;
			}
			return $this->sendResponse($data, 'Success');
		}
		else{
			$roles = ["visitor", "organizer", "exhibitor"];
			foreach($roles as $role){
				$userscount = DB::table('users')->join('user_roles', 'user_roles.user_id', '=', 'users.id')
					->join('roles', 'roles.id', '=', 'user_roles.role_id')->select('users.*')->where('roles.slug', $roles)->count();
				$data["Total No of ".$role] = $userscount;
			}
			$data["Total No of Exibitors Stall"] = DB::table('exhibitor_stalls')->selectRaw('*')->count();
			 
			return $this->sendResponse($data, 'Success');
		}
    	    
		return $this->sendResponse($data, 'No data found.');
    }
    
    
    
}
