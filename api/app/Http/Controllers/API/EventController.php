<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

class EventController extends BaseController
{
    private $pdfFilePath;
    public function __construct(){
        $this->pdfFilePath = "./storage/app/public/pdf/";
    }
    
    public function update_event_document(Request $request){
    
        $params = $request->all();
        //echo '<pre>';print_r($params);exit;
        $validator = Validator::make($request->all(), ['speaker_link' => 'required','slug' => 'required']);
        if ($validator->fails()) {
	       return $this->sendError('Validation Error.', $validator->errors());       
        }
        $postData['speaker_link'] = $params['speaker_link'];
        NULL!==(request('facebook'))?$postData['facebook'] = request('facebook'):'';
        NULL!==(request('instagram'))?$postData['instagram'] = request('instagram'):'';
        NULL!==(request('linkedin'))?$postData['linkedin'] = request('linkedin'):'';
        $postData['modified'] = date('Y-m-d H:i:s');
        //echo '<pre>';print_r($postData);exit;
        //echo '<pre>';print_r($request->all());exit;
        //echo 'reached in api';exit;
        $postData['modified_by']  = $request->user()->id;
        if (!empty($request->hasFile('pdf_file'))) {
            //echo "hiii";print_r($request->hasFile('pdf_file'));exit;
            $original_filename = $request->file('pdf_file')->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $file_ext = (count($original_filename_arr)>1)?end($original_filename_arr):'pdf';
            //$path = $this->pub_html;
            //print_r($file_ext);exit;
            if($file_ext == 'pdf'){
                $image = $original_filename_arr[0].time(). '.'.$file_ext;
                
                if ($request->file('pdf_file')->move($this->pdfFilePath, $image)) {
                    $postData['pdf_file'] = $image;
            	    
                } else {
                    return json_encode(['message'=>'cannot upload file', 'status'=>'fail']);
                    
                }
            }else{
                return json_encode(['message'=>'Enter Valid File Format', 'status'=>'fail']);
              
            }
            
        }
	    //echo '<pre>';print_r($postData);exit;
	    //DB::enableQueryLog();
	    $update = DB::table('events')->where('slug', $params['slug'])->update($postData);
	    //print_r(DB::getQueryLog());
	    if($update){
	        return json_encode(['message'=>'Data Updated Successfully', 'status'=>'success']);
	    }else{
	        return json_encode(['status'=>'fail', 'message'=>'Not Updated']);
	    }
       
       }
       
    public function get_event_detail($slug = NULL){
        if($slug === NULL){
            return json_encode(['status'=>'fail', 'message'=>'unauthorised access']);
        }
        $event = DB::table('events')
        ->join('event_packages', 'event_packages.id','=','events.event_package_id')
        ->where('slug', $slug)->first();
        
        $event->pdf_file_path = $this->pdfFilePath;
        //if($event){
        return json_encode(['status'=>'success', 'data'=>$event]);
        //}
    }
}
