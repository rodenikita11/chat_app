<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

class VisitorLogController extends BaseController
{
    
    public function __construct(){
		
		date_default_timezone_set('Asia/Kolkata'); 
    }
	
	public function exportCsv(Request $request)
	{
	    $fileName = 'visitorlog.csv';
		$visitor_logs = DB::table('visitor_logs')
				->join('users', 'visitor_logs.created_by', '=', 'users.id')
				->leftJoin('exhibitor_stalls', 'visitor_logs.exhibitor_stall_id', '=', 'exhibitor_stalls.exhibitor_id')
				->select('visitor_logs.*','users.name as username', 'exhibitor_stalls.stall_name')
				->orderBy('visitor_logs.created_at', 'ASC')
				->get();
    	
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('id', 'exhibitor_stall_id', 'remark', 'created_at', 'username','stall_name');

        $callback = function() use($visitor_logs, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($visitor_logs as $log) {
                $row['id']  = $log->id;
                $row['exhibitor_stall_id']    = $log->exhibitor_stall_id;
                $row['remark']    = $log->remark;
                $row['created_at']  = $log->created_at;
				$row['username']  = $log->username;
                $row['stall_name']  = $log->stall_name;

                fputcsv($file, array($row['id'], $row['exhibitor_stall_id'], $row['remark'], $row['created_at'], $row['username'], $row['stall_name']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
	
	public function exportCsvStall($stall_id)
	{
	    $fileName = 'visitorlog.csv';
		$visitor_logs = DB::table('visitor_logs')
				->join('users', 'visitor_logs.created_by', '=', 'users.id')
				->join('exhibitor_stalls', 'visitor_logs.exhibitor_stall_id', '=', 'exhibitor_stalls.id')
				->select('visitor_logs.*','users.name as username', 'exhibitor_stalls.stall_name')
				->where('exhibitor_stalls.exhibitor_id', $stall_id)
				->orderBy('visitor_logs.created_at', 'ASC')
				->get();
				//->paginate(10000);
				
    	//var_dump($visitor_logs); die;
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('id', 'exhibitor_stall_id', 'remark', 'created_at', 'username','stall_name');

        $callback = function() use($visitor_logs, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($visitor_logs as $log) {
                $row['id']  = $log->id;
                $row['exhibitor_stall_id']    = $log->exhibitor_stall_id;
                $row['remark']    = $log->remark;
                $row['created_at']  = $log->created_at;
				$row['username']  = $log->username;
                $row['stall_name']  = $log->stall_name;

                fputcsv($file, array($row['id'], $row['exhibitor_stall_id'], $row['remark'], $row['created_at'], $row['username'], $row['stall_name']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
	
    public function index(){
    	$visitor_logs = DB::table('visitor_logs')
				->join('users', 'visitor_logs.created_by', '=', 'users.id')
				->leftJoin('exhibitor_stalls', 'visitor_logs.exhibitor_stall_id', '=', 'exhibitor_stalls.exhibitor_id')
				->select('visitor_logs.*','users.name as username', 'exhibitor_stalls.stall_name')
				->orderBy('visitor_logs.created_at', 'ASC')
				->paginate(10);
    	$data = [];
    	if(count($visitor_logs)>0){
    	    
    	    return $this->sendResponse($visitor_logs, 'Get Visitor Logs data successfully.');
    	}else{
    	    return $this->sendResponse($data, 'No data found.');
    	}
    	
    }

    public function add_visitor_logs(Request $request){
		$id = $request->user()->id;
        $input = $request->all();
    	//echo '<pre>';print_r($input);exit;
        $validator = Validator::make($request->all(), [
            'remark' => 'required'
        ]);
       // echo '<pre>';print_r($validator);exit;
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
		
        if(isset($input['exhibitor_stall_id'])){
			$postData['exhibitor_stall_id'] = $input['exhibitor_stall_id'];
		}
        $postData['remark'] = $input['remark'];
        $postData['created_at'] = date('Y-m-d H:i:s');
		$postData['created_by'] = $id;
        
        //echo '<pre>';print_r($postData);exit;
    	$query = DB::table('visitor_logs')->insertGetId($postData);
    	if($query){
    	    $result = ['message'=>'Visitor Logs created successfully with id - '.$query, 'status'=>'success'];
    	    return json_encode($result);
    	}else{
    	    $result = ['message'=>'error occured while creating Visitor Logs', 'status'=>'fail'];
    	    return json_encode($result);
    	}
    	
    }
    
    public function delete_visitor_logs($id){
        //echo $id;exit;
    	$query = DB::table('visitor_logs')->where('id', $id)->delete();
    	if($query){
    	    $result = ['message'=>'Visitor Logs deleted successfully', 'status'=>'success'];
    	    return json_encode($result);
    	}else{
    	    $result = ['message'=>'error occured while deleting Visitor Logs', 'status'=>'fail'];
    	    return json_encode($result);
    	}
    	
    }
    
    public function get_stall_logs($stall_id){
        //echo $id;exit;
    	$res = DB::table('visitor_logs')
				->join('users', 'visitor_logs.created_by', '=', 'users.id')
				->join('exhibitor_stalls', 'visitor_logs.exhibitor_stall_id', '=', 'exhibitor_stalls.exhibitor_id')
				->select('visitor_logs.*','users.name as username', 'exhibitor_stalls.stall_name')
				->where('exhibitor_stall_id', $stall_id)
				->get();
    	
    	if($res){
    	    $result = ['message'=>'Visitor Logs of stall fetched successfully', 'status'=>'success', 'data'=>$res];
    	    return json_encode($result);
    	}else{
    	    $result = ['message'=>'no data exist', 'status'=>'fail'];
    	    return json_encode($result);
    	}
    	
    }
    
}
