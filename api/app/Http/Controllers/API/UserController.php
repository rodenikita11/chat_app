<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

class UserController extends BaseController
{
    private $pub_html;
    private $profileImagePath;
    public function __construct(){
        $this->profileImagePath = "./storage/app/public/profile_image/";
    }

    public function index($type){
        /*$user = User::paginate(10);
        echo json_encode($user);exit;*/
       $users = DB::table('users')->join('user_roles', 'user_roles.user_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'user_roles.role_id')->select('users.*', 'roles.role_name')->where('roles.slug', $type)->get();
       	$data = [];
       	//print_r($users);exit;
    	if(count($users)>0){
    	    foreach($users as $key=>$user){
    	        $users[$key]->profile_image_path = $this->profileImagePath;
    	    }
    	    return $this->sendResponse($users, 'Success');
    	    //return json_encode(['status'=>'success', 'data'=>$user]);
    	}else{
    	    return $this->sendResponse($data, 'No data found.');
    	}
    }
    
    public function register(Request $request)
    {
        //echo json_encode($request->all());exit;
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'mobile' => 'required',
            'c_password' => 'required|same:password',
            'role' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        if($user){
            $role = DB::table('roles')->select('*')->where('slug', $input['role'])->first();
            $userRole['user_id'] = $user->id;
            $userRole['role_id'] = $role->id;
            $userRole['is_active'] = true;
            $userRole['created_at'] = $userRole['modified_at'] = date('Y-m-d H:i:s');
            $urole = DB::table('user_roles')->insert($userRole);
        }
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;
   
        return $this->sendResponse($success, 'User register successfully.');
    }
    
    public function add_user(Request $request)
    {
        //print_r($request->all());exit;
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'mobile' => 'required',
            'role' => 'required',
            'password' => 'required|min:6',
        ]);
        if($validator->fails()){
            return json_encode(['message'=>'Validation Error Occurred :'.$validator->errors(), 'status'=>'fail']);       
        }
           
        $postData = $request->all();
        //echo '<pre>';print_r($postData);exit;
        $postData['created_at'] = date('Y-m-d H:i:s');
        $postData['created_by'] = $request->user()->id;
        
        if(!empty($request->hasFile('profile_image'))){
            $original_filename = $request->file('profile_image')->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $file_ext = end($original_filename_arr);
            $file_type = $request->file('profile_image')->getMimeType();
            if($file_type == 'image/png' || $file_type == 'image/jpg' || $file_type == 'image/jpeg'){
            	$image = $original_filename_arr[0].time(). '.'.$file_ext;
                if ($request->file('profile_image')->move($this->profileImagePath, $image)) {
                    $postData['profile_image'] = $image;
                } else {
                    return json_encode(['message'=>'cannot upload file', 'status'=>'fail']); 
                }
            }else{
                return json_encode(['message'=>'Enter Valid File Format', 'status'=>'fail']);
            }
        }
        //echo json_encode($postData);exit;
        $postData['password'] = bcrypt($postData['password']);
        //print_r($postData);exit;
        $user = User::create($postData);
        if($user){
            $role = DB::table('roles')->select('*')->where('slug', $postData['role'])->first();
            $userRole['user_id'] = $user->id;
            $userRole['role_id'] = $role->id;
            $userRole['is_active'] = true;
            $userRole['created_at'] = $userRole['modified_at'] = date('Y-m-d H:i:s');
            $urole = DB::table('user_roles')->insert($userRole);
        }
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;
        return json_encode(['message'=>'User Created Successfully', 'status'=>'success']);
        //return $this->sendResponse($success, 'User register successfully.');
    }
    
    public function edit_user(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
            'role' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $input = $request->all();
        $postData['name'] = $input['name'];
        $postData['email'] = $input['email'];
        $postData['mobile'] = $input['mobile'];
        $postData['updated_at'] = date('Y-m-d H:i:s');
        $postData['modified_by'] = $request->user()->id;
        //print_r($postData);exit;
        if (!empty($request->hasFile('profile_image'))){
            //echo "hiii";print_r($request->hasFile('pdf_file'));exit;
            $original_filename = $request->file('profile_image')->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $file_ext = end($original_filename_arr);
            $file_type = $request->file('profile_image')->getMimeType();
            //echo '<pre>';print_r($file_ext);exit;
            $path = $this->pub_html;
            //print_r($file_ext);exit;
           // if($file_ext == 'pdf'){
           if($file_type == 'image/png' || $file_type == 'image/jpg' || $file_type == 'image/jpeg'){
            	$image = $original_filename_arr[0].time(). '.'.$file_ext;
                if ($request->file('profile_image')->move($this->profileImagePath, $image)) {
                    $postData['profile_image'] = $image;
                } else {
                    return json_encode(['message'=>'cannot upload file', 'status'=>'fail']); 
                }
            }else{
                return json_encode(['message'=>'Enter Valid File Format', 'status'=>'fail']);
            }
        }
        //print_r($postData);exit;
        $user = DB::table('users')->where('id', $id)->update($postData);
        if($user){
            $role = DB::table('roles')->select('*')->where('slug', $input['role'])->first();
            if($role){
            
                $checkUserRole = DB::table('user_roles')->select('*')->where(['user_id' => $id, 'role_id'=>$role->id])->first();
                if($checkUserRole){
                  $userRole['modified_at'] = date('Y-m-d H:i:s');
                  $urole = DB::table('user_roles')->where(['user_id' => $id, 'role_id'=>$role->id])->update($userRole);
                }else{
                    $userRole['user_id'] = $id;
                    $userRole['role_id'] = $role->id;
                    $userRole['is_active'] = true;
                    $userRole['created_at'] = $userRole['modified_at'] = date('Y-m-d H:i:s');
                    $urole = DB::table('user_roles')->insert($userRole);
                }
            }
            
            
        }
        //echo 'hello';exit;
        return json_encode(['status'=>'success', 'message'=>'detail updated successfully']);
        
    }
    
    public function view_user($id){
        if($id === NULL){
            return json_encode(['status'=>'fail', 'message'=>'unauthorised access']);
        }
        $user = DB::table('users')->join('user_roles', 'user_roles.user_id','=','users.id')->join('roles', 'roles.id','=', 'user_roles.role_id')
        ->select(['users.id','users.name', 'users.email', 'users.mobile', 'users.profile_image', 'roles.role_name'])
        ->where('users.id', $id)->get();
        //print_r($user);exit;
        if($user[0]){
            $user[0]->profile_image = env('FILE_URL').'profile_image/'.$user[0]->profile_image;
        return json_encode(['status'=>'success', 'data'=>$user[0]]);
        }
    }
    
    public function user_detail(Request $request){
        $id = $request->user()->id;
        //echo $id;
        if($id === NULL){
            return json_encode(['status'=>'fail', 'message'=>'unauthorised access']);
        }
        $user = DB::table('users')->join('user_roles', 'user_roles.user_id','=','users.id')->join('roles', 'roles.id','=', 'user_roles.role_id')
        ->select(['users.id','users.name', 'users.email', 'users.mobile', 'users.profile_image', 'roles.role_name'])
        ->where('users.id', $id)->get();
        //print_r($user);exit;
        if($user[0]){
            $user[0]->profile_image = env('FILE_URL').'profile_image/'.$user[0]->profile_image;
        return json_encode(['status'=>'success', 'data'=>$user[0]]);
        }
    }
    
    public function list(Request $request, $type){
        //echo $type;exit();
        $searchQuery = "";
        $response = array();
        $columnName = 'updated_at';
        $columnSortOrder = "asc";
        $rowperpage = "-1";
        $draw = '1';
        //DB::enableQueryLog();
        $stalls = DB::table('users')
            ->join('user_roles', 'user_roles.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'user_roles.role_id')
            //->where('roles.slug','LIKE', $type)
            //->where('roles.id', 3)
            ->select(['users.id','users.name','users.email','users.mobile','users.is_active', 'users.created_at', 'users.updated_at', 'roles.role_name', 'roles.slug']);
        if($_SERVER['REQUEST_METHOD']=='POST'){
            $postData = $request->post();
            //echo '<pre>';print_r($postData);exit;
            ## Read value
            $draw = $postData['draw'];
            $start = $postData['start'];
            $rowperpage = $postData['length']; // Rows display per page
            $columnIndex = $postData['order'][0]['column']; // Column index
            $columnName = $postData['columns'][$columnIndex]['data']; // Column name
            $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
            $searchValue = $postData['search']['value']; // Search value
            if($searchValue != ''){
               $searchQuery = " AND (t.name like '%".$searchValue."%' or t.email like '%".$searchValue."%' or t.mobile like '%".$searchValue."%' or t.role_name like '%".$searchValue."%')";
            }
        }
        
        $sql = $stalls->toSql();
        //echo $sql;exit;
        //print_r(DB::getQueryLog());exit;
        $records = DB::table('users')
            ->join('user_roles', 'user_roles.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'user_roles.role_id')
            ->where('roles.slug', 'LIKE', $type)
            ->select(['users.id','users.name','users.email','users.mobile','users.is_active', 'users.created_at', 'users.updated_at', 'roles.role_name'])
            ->count();
        //print_r(DB::getQueryLog());exit;
        $totalRecords = $records;
        //echo $totalRecords;exit;
        
        $sql2 = 'Select count(*) as allcount from ('.$sql.') t where 1=1 AND slug like "'.$type.'"'.$searchQuery;
        //echo $sql2;exit;
        $records = DB::select($sql2);
        
        $totalRecordwithFilter = $records[0]->allcount;
        $sql2 = 'Select * from ('.$sql.') t where 1=1 AND slug like "'.$type.'" '.$searchQuery.' order by '.$columnName.' '.$columnSortOrder;
        if ($rowperpage!='-1') {
            $sql2.=' LIMIT '.$start.', '.$rowperpage;
        }
        //echo $sql2;
        $records = DB::select($sql2);
        //echo '<pre>';print_r($records);exit;
        $data = array();
        foreach($records as $recordKey => $record ){
            $checkStall = DB::table('exhibitor_stalls')->where('exhibitor_id', $record->id)->first();
           $data[] = array(
            "sr_no" => $recordKey+1,
            "id"=>$record->id,
            "name"=>$record->name,
            "email"=>$record->email,
            "mobile"=>$record->mobile,
            "role"=>$record->role_name,
            "is_active"=>$record->is_active,
            'stall_count'=>($checkStall)?1:0,
            'bg_video' => ($checkStall)?$checkStall->bg_video:'',
            "created_at"=>($record->created_at!=='0000-00-00 00:00:00')?date('d-m-Y', strtotime($record->created_at)):'',
            "updated_at"=>($record->updated_at!=='0000-00-00 00:00:00')?date('d-m-Y', strtotime($record->updated_at)):'',
            'action'=>'Action'
           ); 
        }
        //echo "<pre>"; print_r($data);exit;
        ## Response
        $response = array(
           "draw" => intval($draw),
           "iTotalRecords" => $totalRecordwithFilter,
           "iTotalDisplayRecords" => $totalRecords,
           "aaData" => $data
        );
        
        return json_encode($response);
        //exit;
    }
    
}
