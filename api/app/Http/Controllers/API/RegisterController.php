<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

class RegisterController extends BaseController
{
    private $profileImagePath;
    public function __construct(){
        $this->profileImagePath = "./storage/app/public/profile_image/";
    }
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
     
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'mobile' => 'required',
            'c_password' => 'required|same:password',
            'role' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        if($user){
            $role = DB::table('roles')->select('*')->where('slug', $input['role'])->first();
            $userRole['user_id'] = $user->id;
            $userRole['role_id'] = $role->id;
            $userRole['is_active'] = true;
            $userRole['created_at'] = $userRole['modified_at'] = date('Y-m-d H:i:s');
            $urole = DB::table('user_roles')->insert($userRole);
        }
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;
   
        return $this->sendResponse($success, 'User register successfully.');
    }
   
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //print_r($request->all());exit;
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            $success['name'] =  $user->name;
            $success['id'] =  $user->id;
            $success['profile_image'] =  $user->profile_image;
            $success['profile_image_path'] = $this->profileImagePath;
            $userRoles = DB::table('user_roles')->join('roles', 'roles.id', "=", "user_roles.role_id")->where('user_id', $user->id)->select('roles.slug')->first();
            //print_r($userRoles);
            $success['roles'] = $userRoles->slug;
            return $this->sendResponse($success, 'User login successfully.');
        } 
        else{ 
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        } 
    }
    
    public function logout (Request $request) {
        $accessToken = auth()->user()->token();
        //echo $accessToken;exit;
        $token= $request->user()->tokens->find($accessToken);
        $token->revoke();
        return response(['status'=>'success','message' => 'You have been successfully logged out.'], 200);
    }
}
