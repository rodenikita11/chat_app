<?php

namespace App\Http\Controllers;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\User;
use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\ChatUser;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ChatController extends BaseController
{
	public function __construct()
    {
		date_default_timezone_set('Asia/Kolkata'); 
    }
    // public function get()
    // {
    //     // get all users except the authenticated one
    //     $contacts = User::where('id', '!=', auth()->id())->get();

    //     // get a collection of items where sender_id is the user who sent us a message
    //     // and messages_count is the number of unread messages we have from him
    //     $unreadIds = ChatMessage::select(\DB::raw('`from` as sender_id, count(`from`) as messages_count'))
    //         ->where('to', auth()->id())
    //         ->where('read', false)
    //         ->groupBy('from')
    //         ->get();

    //     // add an unread key to each contact with the count of unread messages
    //     $contacts = $contacts->map(function($contact) use ($unreadIds) {
    //         $contactUnread = $unreadIds->where('sender_id', $contact->id)->first();

    //         $contact->unread = $contactUnread ? $contactUnread->messages_count : 0;

    //         return $contact;
    //     });

    //     return response()->json($contacts);
    // }
	// public function test_page()
	// {
	// 	$spreadsheet = new Spreadsheet();
	// 	$sheet = $spreadsheet->getActiveSheet();
	// 	$sheet->setCellValue('A1', 'Hello World !');

	// 	$writer = new Xlsx($spreadsheet);
	// 	$writer->save('hello_world.xlsx');
	// }
	public function test_page(Request $request)
    {
		$id = 24;//$request->user()->id;
		$finalResponse=[];

		$sqlqry = 'SELECT chat_unique_id from chat_users group by chat_unique_id having count(*) = 2 and max(user_id = "'.$id.'") > 0';
		$records = DB::select($sqlqry);
		$chat_unique_ids=array_column($records, 'chat_unique_id');
		
		$spreadsheet = new Spreadsheet();
		foreach($chat_unique_ids as $k=>$chat_unique_id){
			
			$messages = DB::table('chat_messages')->join('users', 'chat_messages.from_userid', '=', 'users.id')
					 ->select('chat_messages.*', 'users.name as username', 'users.profile_image as avatar')
					 ->where('chat_messages.chat_unique_id', $chat_unique_id)
					 ->get();
			
			if($messages){
				// Create a first sheet, representing sales data
				$spreadsheet->setActiveSheetIndex($k);

				foreach($messages as $k=>$v){

					$spreadsheet->getActiveSheet()->setCellValue('A"'.$k.'" ', $v['username']);
					$spreadsheet->getActiveSheet()->setCellValue('B"'.$k.'" ', $v['message']);

				}

				// Rename sheet
				$spreadsheet->getActiveSheet()->setTitle($chat_unique_id);

				// Create a new worksheet, after the default sheet
				$spreadsheet->createSheet();

				//$finalResponse[$chat_unique_id] = $messages;
			}		
		}
		$writer = new Xlsx($spreadsheet);
		$writer->save('hello_world.xlsx');
		//return $this->sendResponse($finalResponse, 'Success');
	}
	
    public function getusers(Request $request)
    {
		$id = $request->user()->id;
        
		// get all users except the authenticated one
        //$contacts = User::where('id', '!=', auth()->id())->get();
		$users = User::where('id', '!=', $id)->get()->jsonSerialize();
		
		if(count($users)>0){
    	     return $this->sendResponse($users, 'Success');
    	}else{
    	    return $this->sendResponse($data, 'No data found.');
    	}
        //return response($users, Response::HTTP_OK);        
    }
	
	public function getPublicRoom(Request $request)
    {
		$id = $request->user()->id;
		$role = $request->role;

		// get public chat
		$chat = Chat::where(['chat_type' => "public",'type' => $role])->get();

		if(count($chat)>0){
			return $this->sendResponse($chat, 'Success');
    	}else{
			return json_encode(['success'=>false, 'message'=>'No data found.']);
    	}
        //return response($chat, Response::HTTP_OK);        
    }

    public function getMessagesForRoom(Request $request)
    {
		$id = $request->user()->id;
		$reciver_id = $request->roomid;
				
		$status = $request->status;
		$chat_id = '';
		
		//check room is group
		if(str_contains($reciver_id, 'public')){
			
			$chat_id = $reciver_id;
		}
		else{
			$checkChatforUsers = $this->getChatforUsers($id, $reciver_id);
			
			if(isset($checkChatforUsers[0])){
				$chat_id = $checkChatforUsers[0]->chat_unique_id;
			}
			else{
				return json_encode(['success'=>false, 'message'=>'No Messages']);
			}
		}
		
		if($chat_id){
			// get all messages between the authenticated user and the selected user
			//$messages = ChatMessage::where('chat_unique_id', $input['roomid'])->get();
			if($status){	//only fetch unread msgs
				$messages = DB::table('chat_messages')->join('users', 'chat_messages.from_userid', '=', 'users.id')
					 ->select('chat_messages.*', 'users.name as username', 'users.profile_image as avatar')
					 ->where(['chat_messages.chat_unique_id'=>$chat_id, 'is_read'=>'0'])
					 ->whereNotIn( 'from_userid', [$id])
					 ->get();
				if(count($messages)>0){
					//update status of message as read    
					$isRead = ChatMessage::where(['chat_unique_id'=>$chat_id, 'is_read'=>'0'])->update(['is_read' => '1', 'updated_at' => date('Y-m-d H:i:s')]);
				}
			}
			else{
				$messages = DB::table('chat_messages')->join('users', 'chat_messages.from_userid', '=', 'users.id')
					 ->select('chat_messages.*', 'users.name as username', 'users.profile_image as avatar')
					 ->where('chat_messages.chat_unique_id', $chat_id)
					 ->get();
				
				//update status of message as read    
				$isRead = ChatMessage::where(['chat_unique_id'=>$chat_id, 'is_read'=>'0'])->update(['is_read' => '1', 'updated_at' => date('Y-m-d H:i:s')]);
			}
			
			return $this->sendResponse($messages, 'Success');
		}
    }
	
	public function getMessagesCounts(Request $request)
    {
		$id = $request->user()->id;
		$reciver_ids = $request->roomids;
		$chat_id = '';
		$finalResponse=[];
		foreach($reciver_ids as $reciver_id){
			//check room is group
			if(str_contains($reciver_id, 'public')){
				
				$chat_id = $reciver_id;
			}
			else{
				$checkChatforUsers = $this->getChatforUsers($id, $reciver_id);
				
				if(isset($checkChatforUsers[0])){
					$chat_id = $checkChatforUsers[0]->chat_unique_id;
				}
				else{
					$chat_id = '';	// add this bcoz its pass previous chat to next chat whitch have no chat id created. 
				}
			}
			
			if($chat_id){
				// get all messages between the authenticated user and the selected user
				$messagescounts = DB::table('chat_messages')
					// ->selectRaw('COUNT(chat_messages.*) as count, users.id as userid, chat_messages.chat_unique_id')
					 ->selectRaw('*')
					 ->where(['chat_unique_id'=>$chat_id, 'is_read'=>'0'])
					 ->whereNotIn( 'from_userid', [$id])
					 ->count();
					 
				if($messagescounts){
					$finalResponse[][$reciver_id] = $messagescounts;
					
				}
			}
		}
		return $this->sendResponse($finalResponse, 'Success');
    }
	
	public function getrolecounts(Request $request)
    {
		$id = $request->user()->id;
		$roleids = $request->roleids;
		$finalResponse=[];
		
		foreach($roleids as $roleid){
			$chat_unique_ids = [];
			
			$sqlqry = 'SELECT chat_unique_id,cu.user_id,role_id FROM chat_users as cu LEFT join user_roles as ur on cu.user_id = ur.user_id WHERE cu.user_id != "'.$id.'" and role_id="'.$roleid.'" and chat_unique_id IN ( select chat_unique_id from chat_users group by chat_unique_id having count(*) = 2 and max(user_id = "'.$id.'") > 0)';
			$records = DB::select($sqlqry);
			$chat_unique_ids=array_column($records, 'chat_unique_id');
			
			$public = DB::table('chats')->join('roles', 'chats.type', '=', 'roles.slug')
				 ->select('unique_id')
				 ->where(['chat_type' => 'public', 'roles.id' => $roleid])
				 ->get();
				 
			if(count($public) >0){
				$public_chat_id = $public[0]->unique_id;
				array_push($chat_unique_ids, $public_chat_id);
			}
			$messagescounts = DB::table('chat_messages')
				 ->selectRaw('*')
				 ->whereIn('chat_unique_id', $chat_unique_ids)
				 ->whereNotIn('from_userid', [$id])
				 ->where('is_read', '0')
				 ->count();
				 
			if($messagescounts){
				$finalResponse[][$roleid] = $messagescounts;
			}			
		}
		return $this->sendResponse($finalResponse, 'Success');
    }
	
    public function send(Request $request)
    {
		$id = $request->user()->id;
        $input = $request->message;
		$chat_id = '';

		//check room is group then directly add msg with roomid
		if(str_contains($input['chat_id'], 'public')){
			
			//check user added in chat user otherwise add
			$checkRoomUserAdded = ChatUser::where(['chat_unique_id' => $input['chat_id'],'user_id' => $id])->get();
			//var_dump($checkRoomUserAdded);die();
			if(!isset($checkRoomUserAdded[0])){
				$chatuser = $this->createNewChatUser($input['chat_id'], $id);
			}
			$chat_id = $input['chat_id'];

		}
		else{
			$checkChatforUsers = $this->getChatforUsers($id, $input['chat_id']);
			//var_dump($checkChatforUsers);die;
			
			if(isset($checkChatforUsers[0])){
				$chat_id = $checkChatforUsers[0]->chat_unique_id;
			}
			else{
				$result = $this->createNewChat($id, $input['chat_id']);
				$result = json_decode($result, true);

				if($result['data']){
					$chat_id = $result['data']['unique_id'];
				}
			}
		}
		
        if($chat_id){
			//add msg with chat id
			$message = ChatMessage::create([
				'from_userid' => $id,
				'chat_unique_id' => $chat_id,
				'message' => $input['text'],
				'is_read' => '0',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s'),
				
			]);
			
			return $this->sendResponse($message, 'Success');

		}
		else{
			return json_encode(['success'=>false, 'message'=>'Chat Id not found']);
		}
	}
	
	//--------------internal functions-----------------------------//
	public function createNewChat($sender_id, $reciver_id)
    {
		$randomNum=substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyzABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 4);

        $chat = Chat::create([
			'unique_id' => 'visitor-private-'.$randomNum,
            'type' => 'visitor',
            'chat_type' => 'private'
        ]);
		$chat = json_decode($chat);

		if($chat){

			$chatuser1 = $this->createNewChatUser($chat->unique_id, $sender_id);
			$chatuser2 = $this->createNewChatUser($chat->unique_id, $reciver_id);
			
			//var_dump(json_decode(json_encode($chatuser), true));die('nik');
			if($chatuser1 && $chatuser2){
				return json_encode(['success'=>false, 'data'=>$chat, 'message'=>'Success']);
			}
			else{
				return json_encode(['success'=>false, 'message'=>'Chat users not created']);
			}
		}
		else{
			return json_encode(['success'=>false, 'message'=>'Chat not created']);
		}

    }
	
	public function createNewChatUser($chat_id, $user_id){
		
		$chatuser = ChatUser::insert([
					'chat_unique_id' => $chat_id,
					'user_id' => $user_id,
				]);
		
		return $chatuser;
		
	}
	
	public function getChatforUsers($sender_id, $reciver_id)
    {
		$chat = DB::table('chat_users')
                 ->select('chat_unique_id')
				 ->where('chat_unique_id', 'not like', '%public%')
				 ->whereIn('user_id', array($sender_id, $reciver_id))
				 ->groupBy('chat_unique_id')
				 ->havingRaw("COUNT(distinct user_id) = 2")
                 ->get();
				 
		return $chat;
	}
	
	public function getChatFor(Request $request)
    {
		$id = $request->user()->id;
		
		$chat = DB::table('chat_users')->join('users', 'chat_users.userid', '=', 'users.id')
                 ->select('chat_users.*', 'users.name as username')
                 ->get();

        return response()->json($chat);
    }
}
