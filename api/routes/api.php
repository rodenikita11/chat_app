<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\EventController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\API\RoleController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\ExhibitorController;
use App\Http\Controllers\API\VisitorLogController;
use App\Http\Controllers\API\DashboardController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [RegisterController::class, 'index'])->name('login');
Route::get('login', [RegisterController::class, 'index']);
Route::post('register', [RegisterController::class, 'register'])->name('register');
	Route::get('exportall', [VisitorLogController::class, 'exportCsv']);
	Route::get('exportstall/{id}', [VisitorLogController::class, 'exportCsvStall']);
    Route::get('test_page', [ChatController::class, 'test_page']);



/*NIkita chat module*/
//Route::get('getusers', [ChatController::class, 'getusers']);



Route::middleware('auth:api')->group( function () {
    Route::get('logout', [RegisterController::class, 'logout']);
    Route::post('event', [EventController::class, 'update_event_document']);
    Route::get('getevent/{any}', [EventController::class, 'get_event_detail']);
    /* Role Related Constant Start here */
    Route::get('roles', [RoleController::class, 'index']);
    Route::post('addrole', [RoleController::class, 'add_role']);
    Route::post('editrole', [RoleController::class, 'edit_role']);
    Route::get('deleterole/{id}', [RoleController::class, 'delete_role']);
    Route::get('viewrole/{any}', [RoleController::class, 'view_role']);
    /* Role Related Constant End here */
    /* User Related Constant Start here */
    //Route::get('user/{any}', [UserController::class, 'index']);
   //Route::post('register', [RegisterController::class, 'register'])->name('register');
   Route::post('newuser', [UserController::class, 'add_user'])->name('adduser');
    Route::post('edituser/{id}', [UserController::class, 'edit_user']);
    Route::get('deleteuser/{id}', [UserController::class, 'delete_user']);
    Route::get('viewuser/{any}', [UserController::class, 'view_user']);
    Route::get('userdetail', [UserController::class, 'user_detail']);
    Route::get('user/{any}', [UserController::class, 'list']);
    Route::post('user/{any}', [UserController::class, 'list']);
    /* User Related Constant End here */
    /* Exhibitor Stall Related Constant Start here */
    
    Route::get('stall-list', [ExhibitorController::class, 'index']);
    Route::post('stall-list', [ExhibitorController::class, 'index']);
    Route::post('addstall', [ExhibitorController::class, 'add_stall']);
    Route::post('editstall/{id}', [ExhibitorController::class, 'edit_stall']);
    Route::get('deletestall/{id}', [ExhibitorController::class, 'delete_stall']);
    Route::get('viewstall/{any}', [ExhibitorController::class, 'view_stall']);
    Route::post('reorder-stall', [ExhibitorController::class, 'reorder_stall']);
    Route::post('addstallgroup', [ExhibitorController::class, 'add_stall_group']);    
    
    /* Exhibitor Stall Related Constant End here */
	
	/* Log Related Constant Start here */
    Route::get('logs', [VisitorLogController::class, 'index']);
    Route::post('addlogs', [VisitorLogController::class, 'add_visitor_logs']);
    Route::get('deletelogs/{id}', [VisitorLogController::class, 'delete_visitor_logs']);
    Route::get('stalllogs/{any}', [VisitorLogController::class, 'get_stall_logs']);
	//Route::get('exportall', [VisitorLogController::class, 'exportCsv']);
	//Route::get('exportstall/{id}', [VisitorLogController::class, 'exportCsvStall']);

    /* Log Related Constant End here */
	
	Route::get('dashboard/{any}', [DashboardController::class, 'index']);

	
	/*NIkita chat module*/
	Route::get('getusers', [ChatController::class, 'getusers']);
	Route::post('createNewChat', [ChatController::class, 'createNewChat']);
	Route::post('getMessagesForRoom', [ChatController::class, 'getMessagesForRoom']);
	Route::post('send', [ChatController::class, 'send']);
	Route::post('getPublicRoom', [ChatController::class, 'getPublicRoom']);
	Route::post('getMessagesCounts', [ChatController::class, 'getMessagesCounts']);
	Route::post('getrolecounts', [ChatController::class, 'getrolecounts']);
	Route::get('chatexport', [ChatController::class, 'chatExport']);

	/*NIkita chat module apis End here */

});


