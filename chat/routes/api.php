<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ChatController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('getusers', [ChatController::class, 'getusers']);
// Route::post('getMessagesForRoom', [ChatController::class, 'getMessagesFor']);

// Route::post('send', [ChatController::class, 'send']);

// Route::get('/chat',[ChatController::class, 'index']);
// Route::prefix('item')->group(function (){
//     Route::post('/store',[ChatController::class, 'store']);
//     Route::put('/{id}}',[ChatController::class, 'update']);
//     Route::delete('/{id}]',[ChatController::class, 'destroy']);

//     }
// );
